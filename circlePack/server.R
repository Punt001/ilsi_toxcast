library(shiny)
library(ggraph)
library(igraph)
library(tidyverse)
library(data.table)
#library(dtplyr)



assaySummaryTarget<-setnames(copy(fread("data/Assay_Summary_180918.csv"))[, .(assay_component_endpoint_name, assay_function_type, 
                                                                              assay_design_type, biological_process_target, intended_target_family, 
                                                                              intended_target_family_sub, organism, technological_target_official_symbol)][!assay_function_type %in% 
                                                                                                                                                             c("background control")][!is.na(assay_function_type)][!assay_design_type %in% 
                                                                                                                                                                                                                     c("background reporter", "viability reporter")][!biological_process_target %in% 
                                                                                                                                                                                                                                                                       c("cell death", "cell proliferation", "cytotoxicity")][organism == 
                                                                                                                                                                                                                                                                                                                                "human"][!intended_target_family %in% c("background measurement")], 
                             "assay_component_endpoint_name", "assay")


flags <- fread("data/flags.csv")
zScores <- fread("data/zScores.csv")

foodChemicals<- fread("data/ac50_Matrix_180918.csv")[fread("data/ChemicalInfo.csv")[, `:=`(number = 1)][order(Primary, Secondary, 
                                                                                                              Tertiary)][, `:=`(Secondary = paste(Primary, Secondary, sep = "_"))][, 
                                                                                                                                                                                   `:=`(Tertiary = paste(Secondary, Tertiary, sep = "_"))], 
                                                     on = .(Code), allow.cartesian = TRUE]

foodChemicals<-foodChemicals%>% melt(., id.vars = c("Functional.class", "Code","Name", "Primary", "Secondary", "Tertiary", "number"), value.name = "ac50", variable.name = "assay")

foodChemicals<- foodChemicals[, `:=`(FilternumberTested = sum(!is.na(ac50))), keyby = .(assay, Primary, Secondary, Tertiary)][FilternumberTested > 2]

foodChemicals<- merge(merge(merge(
                foodChemicals, 
                  assaySummaryTarget, all.x = TRUE, all.y = FALSE, 
                  by.x = "assay", by.y = "assay", allow.cartesian = TRUE), 
                      flags, all.x = TRUE, all.y = FALSE, by.x = c("Code", "assay"), 
                      by.y = c("Code", "assay"), allow.cartesian = TRUE), 
                          zScores, all.x = TRUE, all.y = FALSE, by.x = c("Code", "assay"), 
                          by.y = c("Code", "assay"), allow.cartesian = TRUE)


######generating relationgs for the circle pack##### 
relation1<- foodChemicals %>% 
  group_by(Functional.class, .keep_all = TRUE) %>%
  mutate(relationFrom = paste("root", sep ="_"))%>%
  mutate(relationTo = paste("root",Functional.class, sep ="_"))%>%
  mutate(labelsFunctional = paste(Functional.class))%>%
  mutate(relationCount = 0) %>%
  ungroup() %>%
  distinct(relationFrom, relationTo, relationCount, labelsFunctional)%>%
  mutate(percPos = 0)%>%
  mutate(labels = "")%>%
  select(relationFrom, relationTo, relationCount, percPos, labels, labelsFunctional)

relation2<- foodChemicals %>% 
  group_by(Functional.class, Primary) %>%
  mutate(relationFrom = paste("root",Functional.class, sep ="_"))%>%
  mutate(relationTo = paste("root",Functional.class, Primary, sep ="_"))%>%
  mutate(relationCount = sum(number)) %>%
  ungroup() %>%
  distinct(relationFrom, relationTo, relationCount)%>%
  mutate(percPos = 0)%>%
  mutate(labels = "")%>%
  mutate(labelsFunctional =NA)

relation3<- foodChemicals %>% 
  group_by(Functional.class, Primary, Secondary) %>%
  mutate(relationFrom = paste("root",Functional.class, Primary, sep ="_"))%>%
  mutate(relationTo = paste("root",Functional.class, Primary, Secondary, sep ="_"))%>%
  mutate(relationCount = sum(number)) %>%
  ungroup() %>%
  distinct(relationFrom, relationTo, relationCount) %>%
  mutate(percPos = 0)%>%
  mutate(labels = "")%>%
  mutate(labelsFunctional =NA)


#filter z-score lower than 3 
optionsGroups<-foodChemicals%>%
  select(Tertiary)%>%
  mutate(labels = paste(group_indices(., Tertiary))) %>%
  #relation4 %>% 
  arrange(as.numeric(labels))%>%
  mutate(labelsGroups = paste(labels,Tertiary, sep = "_"))%>%
  distinct(labelsGroups)


filterForTargetSubFamily <- function(df, targetSubFamily){ #
  if(targetSubFamily == "No selection"){
    df
  } else filter(df, intended_target_family_sub == targetSubFamily)
}

filterForTarget <- function(df, target){ #
  if(target == "No selection"){
    df
  } else filter(df, technological_target_official_symbol == target)
}



# Define server logic ----
server <- function(input, output, session) {
   updateSelectInput(session, 'selected_groupLabel', choices = optionsGroups$labelsGroups
  )
  
  observe({
    optionsTargetSubFamily<-foodChemicals%>%
    distinct(intended_target_family, intended_target_family_sub)%>%
    filter(intended_target_family == input$targetFamily)%>% #
    add_row(., intended_target_family = as.character(input$targetFamily), intended_target_family_sub = "No selection", .before = 1)
    updateSelectInput(session, 'targetFamilySub',choices = optionsTargetSubFamily$intended_target_family_sub, selected = "No selection")
  })
  
  observe({
    optionsTarget<-foodChemicals%>%
      distinct(intended_target_family, intended_target_family_sub, technological_target_official_symbol)%>%
      filter(intended_target_family == input$targetFamily)%>% #
      filterForTargetSubFamily(., input$targetFamilySub)%>%
      add_row(., intended_target_family = as.character(input$targetFamily), 
              intended_target_family_sub = as.character(input$targetFamilySub), 
              technological_target_official_symbol = "No selection", .before = 1)
    updateSelectInput(session, 'target',choices = optionsTarget$technological_target_official_symbol, selected = "No selection")
  })
  
 
  
#####calculating the percentage of chemicals per chemical that are active in the assays that belong to a specific target family#####
  output$plotHeatmap <- renderPlot({
    colorsHeatmap<-foodChemicals %>%
      filter(zScore>input$zScore | is.na(zScore))%>%
      ####SELECT HERE THE TARGET FAMILY THAT NEEDS TO BE DISPLAYED####
      filter(intended_target_family == input$targetFamily)%>% #
      filterForTargetSubFamily(., input$targetFamilySub)%>% #input$targetFamilySub
      filterForTarget(., input$target)%>%#
      group_by(intended_target_family, intended_target_family_sub,technological_target_official_symbol, Primary, Secondary, Tertiary) %>% # 
      summarise("numberTested"=sum(!is.na(ac50)), "numPosTested"=sum(ac50<1000000, na.rm=TRUE), "flags" = sum(!is.na(flag))) %>%
      as.data.frame()%>%
      mutate(percPos=numPosTested/numberTested*100)%>%
      mutate(labels = paste(group_indices(., Tertiary)))
    
    ggplot(data = colorsHeatmap, aes(x = labels, y =  technological_target_official_symbol)) +
      geom_tile(aes(fill = percPos))+
      facet_grid(intended_target_family ~ . , scales = "free", space = "free")+
      scale_fill_gradient2(low="gray", mid = "orange", high="red", midpoint = 50, guide="colorbar")+
      #scale_x_discrete(limits=labelsOrder)+
      guides(fill=guide_legend(title="% active"))+
      labs(x="Tertiary chemical group",y="Biological target") +
      theme(axis.title.x=element_text(size = 13), #element_blank(),
            axis.text.x= element_text(size = 9, angle = 90),#element_blank(), 
            axis.ticks.x=element_blank(),
            strip.text.x = element_text(size = 13),
            axis.title.y= element_text(size = 13), #element_blank(), #,
            axis.text.y=element_text(size = 9), #element_blank(),
            axis.ticks.y=element_blank(),
            strip.text.y = element_text(size = 13,angle = 90))
  })
  

 output$plotCirclePlot <- renderPlot({
   #relation1, 2, and 3 are defined directly in the global environment as these do not require an input selection 

   colors_groups<-    foodChemicals %>%
     filter(zScore>input$zScore | is.na(zScore))%>%
     ####SELECT HERE THE TARGET FAMILY THAT NEEDS TO BE DISPLAYED####
     filter(intended_target_family == input$targetFamily)%>% #
     filterForTargetSubFamily(., input$targetFamilySub)%>% #input$targetFamilySub
     filterForTarget(., input$target)%>%#
     #filter(intended_target_family_sub == "steroidal") %>%  #
     group_by(intended_target_family, Functional.class, Primary, Secondary, Tertiary) %>% # 
     summarise("numberTested"=sum(!is.na(ac50)), "numPosTested"=sum(ac50<1000000, na.rm=TRUE), "flags" = sum(!is.na(flag))) %>%
     as.data.frame()%>%
     mutate(percPos=numPosTested/numberTested*100)%>%
     mutate(labels = paste(group_indices(., Tertiary))) %>%
     mutate(relationTo = paste("root", Functional.class, Primary, Secondary, Tertiary, sep = '_'))%>%
     select(relationTo, percPos, labels)
   
   relation4<- foodChemicals %>% 
     group_by(Functional.class, Primary, Secondary, Tertiary) %>%
     mutate(relationFrom = paste("root",Functional.class, Primary, Secondary, sep ="_"))%>%
     mutate(relationTo = paste("root",Functional.class, Primary, Secondary, Tertiary, sep ="_"))%>%
     #mutate(relationTo = paste("root",Functional.class, Primary, Secondary, Tertiary, sep ="_"))%>%
     mutate(relationCount = sum(number)) %>%
     ungroup() %>%
     distinct(relationFrom, relationTo, relationCount)%>%
     as.data.frame()%>%
     left_join(., colors_groups, by = c("relationTo"))%>%
     mutate(labelsFunctional =NA) 
   
   relations<- rbind(relation1, relation2, relation3, relation4)
   
   
   
   #####generate graph#####
   #network
   relationVertices<- relations %>% 
     distinct(relationTo, relationCount) %>% 
     add_row(relationTo= "root",  relationCount = 0, .before = 1)
   graph <- graph_from_data_frame(relations, vertices = relationVertices)
   #color and label of circles
   relations<-relations%>%
     add_row(relationTo= "root",  relationCount = 0, percPos = 0, labels = "", labelsFunctional = NA, .before = 1)
   fill<- relations[["percPos"]]#relationVertices[["relationCount"]] #
   labels <- relations[["labels"]]
   
   
   #labelsFunctional<-relations[["labelsFunctional"]]
   
   #labelsFunctional <- 
     if (input$suppress == TRUE) {
       labelsFunctional<-c("")
     } else {
       labelsFunctional<-relations[["labelsFunctional"]]
     }
    # ifelse(N > 3, , relations[["labels"]])###
   
   
   #plotting the network as a circlepack
   ggraph(graph, layout = 'circlepack', weight=relationCount) +
     geom_node_circle(aes(fill=fill))+
     geom_node_text( aes(label=labels, filter=leaf), check_overlap = FALSE) +
     geom_node_label(aes(label = labelsFunctional), repel = TRUE)+
     scale_fill_gradient2( high="red", 
                           guide="colorbar", 
                           #limits=c(0,25),
                           na.value = "gray90")+
     guides(fill=guide_legend(title="average %"))+
     theme_void() 
 
  })
  
  output$table<-DT::renderDataTable({
    #source("circlePack_datatable_fast.R", local = TRUE)
    table<-foodChemicals%>%
      #as.data.frame()%>%
      mutate(labels = paste(group_indices(., Tertiary))) %>%
      mutate(Tertiary = paste(labels, Tertiary, sep = "_"))%>%
      filter(intended_target_family== input$targetFamily) %>%#
      filterForTargetSubFamily(., input$targetFamilySub)%>%
      filterForTarget(., input$target)%>%
      filter(Tertiary == input$selected_groupLabel)%>%
      select(Name, Assay = assay, AC50 = ac50, Zscore = zScore, Flag = flag, Target = technological_target_official_symbol)
    
    
  })
  
  
}