library(shiny)

ui <- fluidPage(
  fluidRow( 
    column(6,
           splitLayout(
           selectInput("input",label = "Upload", choices =c("Food Chemical Dataset"), selected = "a"),
           numericInput("zScore", label = "Filter Z-scores lower than:", value = 3)
           ),
           
           tabsetPanel(
             tabPanel("Heatmap", plotOutput("plotHeatmap", width = "800px", height = "430px")),
             tabPanel("Circle Pack", plotOutput("plotCirclePlot", width = "500px", height = "430px"),
                      checkboxInput("suppress", label = "Suppress labels", value = FALSE))
           ),
           splitLayout(
             selectInput("targetFamily",label = "Target Family", choices =c("DNA binding" = "dna binding", "Nuclear receptor" = "nuclear receptor"), selected = "nuclear receptor"),
             selectInput("targetFamilySub",label = "Target Subfamily", choices = NULL, selected = NULL),
             selectInput("target",label = "Target", choices = NULL, selected = NULL)

             
           ),
           hr()
    )
  ),
  fluidRow( 
    column(6, 
           selectInput("selected_groupLabel", label = "Select group:", choices = NULL, selected = NULL),
           DT::dataTableOutput("table")
    )
  )
)
